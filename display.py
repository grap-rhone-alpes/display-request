from werkzeug.wrappers import Request, Response


@Request.application
def application(request):
    res = request.__dict__.copy()
    res['url_root'] = request.url_root
    fromage = ""
    for k, v in res.iteritems():
        fromage += "%s : %s " % (k, v)

    return Response(fromage)

if __name__ == '__main__':
    from werkzeug.serving import run_simple
    run_simple('localhost', 4000, application)
